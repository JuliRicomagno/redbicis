# Bicicletas
🔙🔚 NodeJs, Express, Passport and Mongo


## 🛣️ Routes

[Login ](https://red-cletas.herokuapp.com/) https://red-cletas.herokuapp.com/

Bicicletas is API to rent bikes around the city.

## 📢 Requirements
- Node JS 12.16.1 _(Recommended)_
- NPM 6.13.4 _(Recommended)_


## 🛠 Installation
1. Clone this project.
2. Change of directory to the root of the project.
3. Install the dependencies.
```bash
  npm install
```
4. Run it locally
```bash
  npm run devstart
```

## 🔧 Built with
- [pug](https://pugjs.org/api/getting-started.html) 
- [passport](http://www.passportjs.org)
- [mongoose](https://mongoosejs.com)
- [nodemailer](https://nodemailer.com/about)
- [jasmine](https://nodemailer.com/about) _(Development)_
- [nodemon](https://nodemailer.com/about) _(Development)_


## Authors

- **[Julian Ricomagno - BitBucket](https://bitbucket.org/JuliRicomagno/)** _(Developer)_