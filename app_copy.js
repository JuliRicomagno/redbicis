var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config;

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var tokenRouter = require('./routes/token');
var usuariosRouter = require('./routes/users');
var bicicletasApiRouter = require('./routes/api/bicicletasApi');
//var usuariosApiRouter = require('./routes/api/usuarios');
//var authApiRouter = require('./routes/api/auth');

const passport = require('./config/passport');
const session = require('express-session');
//const MongoDbStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');
const Usuario = require('./models/usuarioModel');

const assert = require('assert');

var app = express();

var mongoose = require('mongoose');

//var mongoDB = 'mongodb://localhost/red_bicicletas';
//mongodb+srv://admin:<password>@red-bicicletas.rpgkq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

var mongoDB = 'mongodb+srv://admin:node2021@red-bicicletas.rpgkq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());


app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', bicicletasRouter);
app.use('/token', tokenRouter);
app.use('/usuarios', usuariosRouter);
app.use('/api/bicicletasApi', bicicletasApiRouter);

//app.use('/api/usuarios', usuariosApiRouter);
//app.use('/api/auth', authApiRouter);
app.get('/login', (req, res, next) => {
  res.render('session/login');
});

app.get('/privacy_policy', (req, res) => {
  res.sendFile(path.join(__dirname, './public', 'privacy_policy.html'));
});

app.get('/google87cb492eb3be84c4.html', (req, res) => {
  res.sendFile(path.join(__dirname, './public', 'google87cb492eb3be84c4.html'));
});

app.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.render('session/login', { info });
    }
    req.logIn(user, (err) => {
      if (err) {
        return next(err);
      }
      res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', function (req, res) {
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function (req, res) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) return res.render('forgotPassword', { info: { message: 'No existe el email para un usuario existente.' } });

    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log('forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function (req, res, next) {
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token) return res.status(400).send({ type: 'not-verifified', msg: 'No existe un usuario asociado al token. Verifique que su token no haya expirado' });

    Usuario.findById(token._userId, function (err, usuario) {
      if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token' });
      res.render('session/resetPassword', { errors: {}, usuario: usuario });
    });
  });
});

app.post('/resetPassword', function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {
      errors: { confirm_password: { message: 'No coincide con el password ingresado' } },
      usuario: new Usuario({ email: req.body.email })
    });
    return;
  }
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });
      }
      else {
        res.redirect('login');
      }
    });
  });
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
